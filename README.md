# Wakdata - REST API - Java - Client

[![Pipeline Status](https://gitlab.com/MathiusD/wakdata-rest-api-java-client/badges/master/pipeline.svg)](https://gitlab.com/MathiusD/wakdata-rest-api-java-client/-/pipelines)

Java client for [Wakdata REST API](https://gitlab.com/MathiusD/wakdata-rest-api-crystal). Package are generated from swagger specification of Wakdata REST API

## Versions

You can found versions of this package here : <https://gitlab.com/MathiusD/wakdata-rest-api-java-client/-/packages>

Version format are following : `majorVersionOfClient.minorVersionOfClient-majorVersionOfSpec.minorVersionOfSpec.patchNumberOfSpec(-SNAPSHOT)?`

Versions used follow version of [Wakdata REST API](https://gitlab.com/MathiusD/wakdata-rest-api-crystal) specified in `swagger.json`
Regular version are configured to interact by default with <https://json.wakdata.waklab.fr/api/v1/swagger>
Snapshot version are configured to interact by default with <https://develop.json.wakdata.waklab.fr/api/v1/swagger>

## Basic example

### Configure dependency

#### Maven

In your `pom.xml` you can add following repository :

```xml
<repositories>
  <repository>
    <id>wakdata-maven-repo</id>
    <url>https://gitlab.com/api/v4/projects/56600849/packages/maven</url>
  </repository>
</repositories>
```

And you can add dependency like this :

```xml
<dependency>
  <groupId>org.waklab.wakdata</groupId>
  <artifactId>api-client</artifactId>
  <!-- Use any version available in https://gitlab.com/MathiusD/wakdata-rest-api-java-client/-/packages -->
  <version>0.1-0.2.2-SNAPSHOT</version>
</dependency>
```

More details can be found on each version in <https://gitlab.com/MathiusD/wakdata-rest-api-java-client/-/packages> or in <https://docs.gitlab.com/ee/user/packages/maven_repository/>

#### Gradle

##### In Groovy

You can add following repository :

```kotlin
implementation("org.waklab.wakdata:api-client:0.1-0.2.2-SNAPSHOT")
```

And you can add dependency like this :

```kotlin
maven("https://gitlab.com/api/v4/projects/56600849/packages/maven")
```

More details can be found on each version in <https://gitlab.com/MathiusD/wakdata-rest-api-java-client/-/packages> or in <https://docs.gitlab.com/ee/user/packages/maven_repository/>

##### In Kotlin

You can add following repository :

```groovy
implementation 'org.waklab.wakdata:api-client:0.1-0.2.2-SNAPSHOT'
```

And you can add dependency like this :

```groovy
maven {
  url 'https://gitlab.com/api/v4/projects/56600849/packages/maven'
}
```

More details can be found on each version in <https://gitlab.com/MathiusD/wakdata-rest-api-java-client/-/packages> or in <https://docs.gitlab.com/ee/user/packages/maven_repository/>

### Basic main

```java
import org.waklab.wakdata.api.ItemsApi;
import org.waklab.wakdata.invoker.ApiException;

public class Main {
    public static void main(String[] args) throws ApiException {
        final ItemsApi itemsApi = new ItemsApi();
        System.out.println(itemsApi.versionItemsGet("latest"));
    }
}
```
